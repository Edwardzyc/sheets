class CreateCells < ActiveRecord::Migration[5.2]
  def change
    create_table :cells do |t|
      t.references :sheet
      t.string :value
      t.string :row
      t.string :column
      t.timestamps
    end
  end
end
