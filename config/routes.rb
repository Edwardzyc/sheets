Rails.application.routes.draw do
  resources :sheets, only: [:index, :show] do
    patch '/cells', to: 'cells#update'
    get '/cells', to: 'cells#show'
  end
  root 'sheets#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
