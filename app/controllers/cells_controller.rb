class CellsController < ApplicationController
  skip_forgery_protection

  def update
    # When user feature logged in, must make sure user can only update sheets/cells
    # that belong to themself
    column, row = params[:cell][:coordinate].split(':')
    @cell = Cell.find_or_initialize_by(sheet_id: params[:sheet_id], column: column, row: row)
    if @cell.new_record?
      @cell.value = params[:cell][:value]
      @cell.save
    else
      @cell.update(value: params[:cell][:value])
    end
  end

  def show
    column, row = params[:cell][:coordinate].split(':')
    @cell = Cell.find_by(sheet_id: params[:sheet_id], column: column, row: row)
    # add serializer
    json = @cell.as_json
    json["computed_value"] = @cell.computed_value
    render json: json
  end

end
