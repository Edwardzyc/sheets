class SheetsController < ApplicationController
  def index
    if Sheet.count.zero?
      sheet = Sheet.create(name: 'My first sheet')
      return redirect_to sheet
    end
    @sheets = Sheet.all
  end

  def show
    @sheet = Sheet.includes(:cells).find(params[:id])
  end
end
