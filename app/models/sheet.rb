class Sheet < ApplicationRecord
  validates :name, presence: true
  has_many :cells
end
