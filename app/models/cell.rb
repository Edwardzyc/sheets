class Cell < ApplicationRecord
  belongs_to :sheet

  validates :row, :column, presence: true

  def is_formula?
    value.start_with?('=')
  end

  def computed_value
    # if is_formula?
    #   # handle other methods. Currently deals with addition only
    #   formula = Formula.new(self)
    #   return formula.add
    # else
    return value
    # end
  end
end
