class Formula
  def initialize(cell)
    @cell = cell
    @parameters = cell.value[1..-1]
  end

  def add
    begin
      x, y = @parameters.split('+')
      return computed_parameter(x).to_i + computed_parameter(y).to_i  
    rescue StandardError => e
      return 'Error evaluating formula'
    end
  end

  def computed_parameter(parameter)
    if parameter_references_cell?(parameter)
      column, row = parameter.split(':')
      cell = Cell.find_by(sheet_id: @cell.sheet_id, column: column, row: row)
      cell.nil? ? '' : cell.value
    else
      return parameter
    end
  end


  def parameter_references_cell?(parameter)
    return parameter.include?(':')
  end

end