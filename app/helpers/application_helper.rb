module ApplicationHelper
  def cell_id(sheet, column, row)
    cell = sheet.cells.to_a.find {|cell| cell.column == column && cell.row == row }
    if cell
      return "data-cell-id=#{cell.id}"
    else
      return nil
    end
  end

  def get_cell(sheet, column, row)
    sheet.cells.to_a.find {|cell| cell.column == column && cell.row == row }
  end

  def cell_value(cell)
    if cell
      return cell.computed_value
    else
      return nil
    end
  end

  def cell_formula(cell)
    if cell
      cell.is_formula? ? "data-formula=true" : nil
    else
      nil
    end
  end

  def coordinate(column, row)
    return "#{column}:#{row}"
  end
end
