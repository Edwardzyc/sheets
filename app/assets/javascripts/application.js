// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery
//= require_tree .

$(document).ready(() => {
  if ($('.sheet').length > 0) {

  }

  const sheet = {
    sheetID: $('.sheet').data('sheet-id'),
  }


  const CELLS = {}

  function updateCell(params) {
    return $.ajax({ type: "PATCH", url: `/sheets/${sheet.sheetID}/cells`, data: params })
  }

  // addition only
  function add($cell) {
    return splitAddFormulaToParameters($cell)
      .map(value => {
        const ret = value.includes(':') ? $(`.cell[data-coordinate='${value}']`).val() : value
        return parseInt(ret)
      }).reduce((sum, current) => sum + current)
  }

  function splitAddFormulaToParameters($cell) {
    const formula = CELLS[$cell.data('coordinate')]['value']
    return formula.slice(1).split('+')
  }

  $('.sheet').on('focusin', '.cell', function(e) {
    const $cell = $(this)
    const coordinate = $cell.data('coordinate')
    $cell.val(CELLS[coordinate]['value'])
  })

  function computeValue($cell) {
    var value = CELLS[$cell.data('coordinate')]['value'] 
    if (value.charAt(0) === "=") {
      return add($cell)
    } else { 
      return isNaN(parseInt(value)) ? value : parseFloat(value); 
    }
  }

  $('.sheet').on('blur', '.cell', function(e) {
    const $cell = $(this), coordinate = $cell.data('coordinate'), value = $cell.val()
    CELLS[coordinate]['value'] = value
    $cell.val(computeValue($cell))
  })

  $('.sheet').on('change', '.cell', function(e) {
    const $cell = $(this), coordinate = $cell.data('coordinate'), value = $cell.val()
    const params = { 
      'cell': { 
        'coordinate': coordinate, 'value': value
      } 
    }
    updateCell(params).done(() => flickerCell($cell))
    recalculateOtherCells($cell)
  })

  function recalculateOtherCells($changedCell) {
    for (const [coordinate, item] of Object.entries(CELLS)) {
      if (item['value'].charAt(0) === "=") {
        const $currentCell = $(`.cell[data-coordinate='${coordinate}']`)
        const found = splitAddFormulaToParameters($currentCell).some((currentCellCoordinate) => {
          return currentCellCoordinate === $changedCell.data('coordinate')
        })
        if (found) {
          $currentCell.val(computeValue($currentCell))
          $currentCell.change()
        }
      }
    }
  }

  function flickerCell($cell) {
    $cell.addClass('success-border')
    setTimeout(() => $cell.removeClass('success-border'), 750);
  }

  function setup() {
    $('.cell').each(function(index, cell) {
      const $c = $(cell)
      CELLS[$c.data('coordinate')] = {
        value: $c.val()
      }
      $c.val(computeValue($c))
    })
  }

  setup()
})